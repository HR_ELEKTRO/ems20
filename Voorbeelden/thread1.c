#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ti/sysbios/BIOS.h>
#include "ti_drivers_config.h"

/* This program shows how two threads can run concurrent. */

void *taak1(void *par) {
    for (int i = 0; i < 10; i++) {
        usleep(100000);
        puts("Taak 1 zegt: hallo");
    }
    return NULL;
}

void *taak2(void *par) {
    for (int i = 0; i < 10; i++) {
        usleep(200000);
        puts("Taak 2 zegt: ook hallo");
    }
    return NULL;
}

int main(void) {
    Board_init();
    pthread_t t1, t2;
    pthread_create(&t1, NULL, &taak1, NULL);
    pthread_create(&t2, NULL, &taak2, NULL);
    puts("Ready... Go!");
    BIOS_start();
    return 0;
}
