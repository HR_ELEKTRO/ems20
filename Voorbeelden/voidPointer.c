int main(void) {
    int i = 3;
    double d = 4.3;

    void* vp = &i;
    printf("%d\n", *(int*)vp);
    vp = &d;
    printf("%lf\n", *(double*)vp);

    return 0;
}
