#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <mqueue.h>
#include <ti/sysbios/BIOS.h>
#include "ti_drivers_config.h"

void *producer(void *p) {
    mqd_t mq = *(mqd_t *)p;
    for (int i = 0; i < 10; i++) {
        mq_send(mq , (char *)&i, sizeof(i), 0);
    }
    return NULL;
}

void *consumer(void *p) {
    mqd_t mq = *(mqd_t *)p;
    while (1) {
        int msg;
        mq_receive(mq, (char *)&msg, sizeof(msg), NULL);
        printf("%d\n", msg);
    }
}

int main(void) {
    Board_init();

    mqd_t mqdes;
    mq_attr mqAttrs;
    mqAttrs.mq_maxmsg = 3;
    mqAttrs.mq_msgsize = sizeof(int);
    mqAttrs.mq_flags = 0;
    mqdes = mq_open("ints", O_RDWR | O_CREAT, 0666, &mqAttrs);

    pthread_t tp1, tp2, tc;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setstacksize(&attr, 1024);
    pthread_create(&tp1, &attr, &producer, &mqdes);
    pthread_create(&tp2, &attr, &producer, &mqdes);
    pthread_create(&tc, &attr, &consumer, &mqdes);

    puts("Ready... Go!");
    BIOS_start();
    return 0;
}
