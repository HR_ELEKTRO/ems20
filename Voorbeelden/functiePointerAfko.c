#include <stdio.h>

int kwadraat(int c) {
    return c * c;
}

int dubbel(int c) {
    return c + c;
}

int main(void) {
    int a = 7, b;
    int (*pnf)(int);

    pnf = dubbel;
    b = pnf(a);
    printf("b = %d\n", b);

    pnf = kwadraat;
    b = pnf(a);
    printf("b = %d\n", b);

    return 0;
}
