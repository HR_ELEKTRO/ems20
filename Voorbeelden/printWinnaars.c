#include <stdio.h>

typedef struct {
    char naam[80];
    int punten;
} Deelnemer;

typedef struct {
    Deelnemer speler[50];
    int aantalSpelers;
} Stand;

Stand vulStand(void)
{
    Stand st = {
        {
            {"Marie-Louise", 16},
            {"Harry", 24},
            {"Anne", 12},
            {"Theo", 17},
            {"Koen", 20},
            {"Linda", 24}
        }, 6
    };
    return st;
}

void printWinnaars(Stand st) {
    if (st.aantalSpelers == 0) {
        printf("Er is geen winnaar.\n");
    }
    else {
        int i, max;
        max = st.speler[0].punten;
        for (i = 1; i < st.aantalSpelers; i++) {
            if (st.speler[i].punten > max) {
                max = st.speler[i].punten;
            }
        }
        printf("De winnaar(s) is(zijn):\n");
        for (i = 0; i < st.aantalSpelers; i++) {
            if (st.speler[i].punten == max) {
                printf("%s\n", st.speler[i].naam);
            }
        }
    }
}

int main(void)
{
    Stand s;
    s = vulStand();
    printWinnaars(s);

    while (1);
}
