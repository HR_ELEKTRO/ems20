#include <stdio.h>

typedef enum {MA, DI, WO, DO, VRIJ, ZAT, ZON} dag;
const char * dagnaam[] = {
    "maandag", "dinsdag", "woensdag",
    "donderdag", "vrijdag", "zaterdag", "zondag"
};

int main(void)
{
    dag afspraak = DO;

    printf("De afspraak is op %s!", dagnaam[afspraak]);

    while(1);
    return 0;
}
