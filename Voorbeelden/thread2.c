#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ti/sysbios/BIOS.h>
#include "ti_drivers_config.h"

/* This program shows how to pass arguments to a thread. */

typedef struct {
    char *msg;
    long us;
    int times;
} par_t;

void *taak(void* pp) {
    par_t p = *(par_t *)pp;
    for (int i = 0; i < p.times; i++) {
        usleep(p.us);
        puts(p.msg);
    }
    return NULL;
}

int main(void) {
    Board_init();
    pthread_t t1, t2;
    par_t p1 = {"Taak 1 zegt: hallo", 100000, 9};
    par_t p2 = {"Taak 2 zegt: ook hallo", 200000, 11};
    pthread_create(&t1, NULL, &taak, &p1);
    pthread_create(&t2, NULL, &taak, &p2);
    puts("Ready... Go!");
    BIOS_start();
    return 0;
}
