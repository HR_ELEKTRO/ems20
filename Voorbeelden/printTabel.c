#include <stdio.h>

int kwadraat(int c) {
    return c * c;
}

int dubbel(int c) {
    return c + c;
}

void printTabel(int (*p)(int), int van, int tot, int stap) {
    for (int x = van; x < tot; x += stap) {
        printf("%10d %10d\n", x, (*p)(x));
    }
}

int main(void) {
    printf("De kwadraten van 1 t/m 10\n");
    printTabel(&kwadraat, 1, 11, 1);
    printf("De dubbelen van de drievouden van 0 t/m 30\n");
    printTabel(&dubbel, 0, 31, 3);
    return 0;
}
