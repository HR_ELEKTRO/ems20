/*
 * Copyright (c) 2024, Rotterdam University of Applied Sciences
 * All rights reserved.
 */

#include <stdint.h>
// Driver Header files
#include <NoRTOS.h>
#include <ti/drivers/GPIO.h>
// Driver configuration
#include "ti_drivers_config.h"

int main(void) {
    Board_init();
    NoRTOS_start();
    // Turn off red led
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_OFF);

    while (1) { // Loop forever blinking
        volatile int count = 0;
        for (; count < 1000000; count++) /* wait */;
        GPIO_toggle(CONFIG_GPIO_LED_0);
    }
}
