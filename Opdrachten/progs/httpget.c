#include <stdio.h>
#include <string.h>
#include <ti/display/Display.h>
#include <ti/net/http/httpclient.h>

#define HOSTNAME      "http://www.meteo-oudetonge.nl"
#define REQUEST_URI   "/updates/starttekst.html"
#define USER_AGENT    "HTTPClient (ARM; TI-RTOS)"
#define HTTP_MIN_RECV (333)

extern Display_Handle display;
extern void printError(char *errString, int code);

void* httpTask(void* pvParameters)
{
    Display_printf(display, 0, 0, "Sending a HTTP GET request to '%s'", HOSTNAME);
    int16_t statusCode;
    HTTPClient_Handle httpClientHandle = HTTPClient_create(&statusCode, 0);
    if (statusCode < 0) {
        printError("httpTask: creation of http client handle failed", statusCode);
    }
    int16_t ret = HTTPClient_setHeader(
        httpClientHandle, HTTPClient_HFIELD_REQ_USER_AGENT, USER_AGENT, strlen(USER_AGENT) + 1, HTTPClient_HFIELD_PERSISTENT);
    if (ret < 0) {
        printError("httpTask: setting request header failed", ret);
    }
    ret = HTTPClient_connect(httpClientHandle, HOSTNAME, 0, 0);
    if (ret < 0) {
        printError("httpTask: connect failed", ret);
    }
    ret = HTTPClient_sendRequest(httpClientHandle, HTTP_METHOD_GET, REQUEST_URI, NULL, 0, 0);
    if (ret < 0) {
        printError("httpTask: send failed", ret);
    }
    if (ret != HTTP_SC_OK) {
        printError("httpTask: cannot get status", ret);
    }
    Display_printf(display, 0, 0, "HTTP Response Status Code: %d", ret);
    bool moreDataFlag = false;
    char data[HTTP_MIN_RECV];
    int32_t len = 0;
    do
    {
        ret = HTTPClient_readResponseBody(httpClientHandle, data, sizeof data - 1, &moreDataFlag);
        if (ret < 0) {
            printError("httpTask: response body processing failed", ret);
        }
        data[ret] = '\0';
        const char *zoektekst1 = "Temperatuur:</FONT></STRONG></TD>";
        char *p1 = strstr(data, zoektekst1);
        if (p1 != NULL) {
            const char *zoektekst2 = "size=\"2\">";
            char *p2 = strstr(p1, zoektekst2);
            if (p2 != NULL) {
                float temperature;
                if (sscanf(p2 + strlen(zoektekst2), "%f", &temperature) == 1) {
                    char buffer[100];
                    snprintf(buffer, sizeof buffer, "Temperature in Oude-Tonge: %3.1f degrees Celsius", temperature);
                    Display_printf(display, 0, 0, "%s", buffer);
                }
            }
        }
        len += ret;
    }
    while (moreDataFlag);
    Display_printf(display, 0, 0, "Received %d bytes of payload", len);
    ret = HTTPClient_disconnect(httpClientHandle);
    if (ret < 0) {
        printError("httpTask: disconnect failed", ret);
    }
    HTTPClient_destroy(httpClientHandle);
    return NULL;
}

