#include <stdint.h>
#include <stddef.h>

#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART2.h>

#include "ti_drivers_config.h"

void *mainThread(void *arg0)
{
    GPIO_init();

    /* Create a UART where the default read and write mode is BLOCKING */
    UART2_Params uartParams;
    UART2_Params_init(&uartParams);
    uartParams.baudRate = 115200;

    UART2_Handle uart = UART2_open(CONFIG_UART2_0, &uartParams);

    if (uart == NULL) {
        /* UART2_open() failed */
        while (1);
    }

    /* Turn on user LED to indicate successful initialization */
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_ON);

    const char echoPrompt[] = "Echoing characters:\r\n";
    size_t bytesWritten;
    UART2_write(uart, echoPrompt, sizeof(echoPrompt), &bytesWritten);

    /* Loop forever echoing */
    while (1) {
        char input;
        size_t bytesRead = 0;
        while (bytesRead == 0) {
            uint32_t status = UART2_read(uart, &input, 1, &bytesRead);

            if (status != UART2_STATUS_SUCCESS) {
                /* UART2_read() failed */
                 while (1);
            }
        }

        size_t bytesWritten = 0;
        while (bytesWritten == 0) {
            uint32_t status = UART2_write(uart, &input, 1, &bytesWritten);

            if (status != UART2_STATUS_SUCCESS) {
                /* UART2_write() failed */
                while (1);
            }
        }
    }
}
