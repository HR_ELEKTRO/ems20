import socket

HOST = '192.168.192.53' # The server's IP address
PORT = 2000 # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    data = s.recv(100)
    print('Received:', data.decode('Cp1252'))
        