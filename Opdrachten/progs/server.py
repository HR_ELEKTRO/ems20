import socket

HOST = '0.0.0.0' # accept any IP-adres
PORT = 2000 # Port to listen on

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    while True:
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            conn.sendall(b'Harry Broeders\0')
