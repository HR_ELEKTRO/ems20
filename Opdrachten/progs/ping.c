#include <ti/display/Display.h>
#include <ti/drivers/net/wifi/slnetifwifi.h>

extern Display_Handle display;

void* pingTask(void* pvParameters)
{
    SlNetAppPingCommand_t pingCommand;
    pingCommand.Ip = SL_IPV4_VAL(93,184,216,34);  /* destination IP address is 93.184.216.34 (example.com) */
    pingCommand.PingSize = 32;                    /* size of ping, in bytes */
    pingCommand.PingIntervalTime = 100;           /* delay between pings, in milliseconds */
    pingCommand.PingRequestTimeout = 1000;        /* timeout for every ping in milliseconds */
    pingCommand.TotalNumberOfAttempts = 4;        /* number of ping requests */
    pingCommand.Flags = 0;                        /* report only when finished */
    SlNetAppPingReport_t report;
    _i16 status = sl_NetAppPing( &pingCommand, SL_AF_INET, &report, NULL );
    if (status < 0) {
        while (1); /* error */
    }
    Display_printf(display, 0, 0, "Packets sent: %d", report.PacketsSent);
    Display_printf(display, 0, 0, "Packets received: %d", report.PacketsReceived);
    Display_printf(display, 0, 0, "Average round trip time: %d ms", report.AvgRoundTime);

    return NULL;
}
