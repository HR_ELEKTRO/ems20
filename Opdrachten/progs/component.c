#include <stdio.h>

typedef struct {
    int type;
    int index;
    double waarde;
} PassieveComponent;

void printComponent(PassieveComponent c)
{
    switch (c.type)
    {
    case 1:
        printf("R%d = %.1E ohm", c.index, c.waarde);
        break;
    case 2:
        printf("C%d = %.1E farad", c.index, c.waarde);
        break;
    case 3:
        printf("L%d = %.1E henry", c.index, c.waarde);
        break;
    }
}

int main(void)
{
    PassieveComponent c[] = {
        {1, 1, 1E6}, {1, 2, 2.7E3}, {2, 1, 1E-9}, {3, 1, 1E-3}, {2, 2, 15E-6}
    };

    printf("\n");
    for (size_t i = 0; i < sizeof c / sizeof c[0]; i++)
    {
        printComponent(c[i]);
        printf("\n");
    }

    while (1);
    return 0;
}
