// Voor sleep en printf
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>

/* POSIX Header files */
#include <pthread.h>

/* RTOS header files */
#include <ti/sysbios/BIOS.h>

/* Driver Header files */
#include <ti/drivers/Board.h>
#include <ti/drivers/GPIO.h>
#include "ti_drivers_config.h"

void *mijnTaakFunctie(void *arg) {
    while (1) {
        //Doe iets
        puts("Hallo!\n"); //put string (domme versie van printf)
        sleep(1); //slaap 1s
    }
}

int main() {
    pthread_t mijnTaakHandle;

    Board_init(); //TI Drivers
    pthread_create(&mijnTaakHandle, NULL, &mijnTaakFunctie, NULL);
    BIOS_start(); //TI-RTOS

    return 0;
}
