#include <msp430.h>

extern int mul(int x, int n);

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;
    
    volatile int a = 0;

    a = mul(130, 140);

    while (1);
}
