void busySleep(uint8_t n)
{	//8e6 is ongeveer een seconde
	volatile uint32_t counter = 8e6;
	int i;
	for(i=0; i<n; i++){
		while(counter--);
		counter = 8e6;
	}
}