        .def    mul
        .text
mul     clr.w   r14
next    clrc
        rrc.w   r12
        jnc     no_add
        add.w   r13,r14
no_add  add.w   r13,r13
        tst.w   r12
        jne     next
        mov.w   r14,r12
        ret
