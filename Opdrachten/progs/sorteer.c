#include <stdio.h>

void sorteer(int a, int b)
{
    if (a > b)
    {
        int hulpje = a;
        a = b;
        b = hulpje;
    }
}

int main(void)
{
    int x1 = 7, x2 = 8;
    sorteer(x1, x2);
    if (x1 == 7 && x2 == 8)
    {
        printf("Test 1 geslaagd!\n");
    }
    else
    {
        printf("Test 1 NIET geslaagd!\n");
    }
    int x3 = 8, x4 = 7;
    sorteer(x3, x4);
    if (x3 == 7 && x4 == 8)
    {
        printf("Test 2 geslaagd!\n");
    }
    else
    {
        printf("Test 2 NIET geslaagd!\n");
    }
    return 0;
}
