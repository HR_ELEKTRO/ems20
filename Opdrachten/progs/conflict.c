volatile int counter = 0;

void *TelOp(void *arg) {
    int i;
    for (i = 0; i < 1e6; i++) {
        counter++;
    }
}

void *TelOp2(void *arg) {
    int i;
    usleep(1000);
    for (i = 0; i < 1e6; i++)
    {
        counter++;
    }

    sleep(1);
    printf("counter: %d\n", counter);
}