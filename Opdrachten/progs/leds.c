/*
 * Copyright (c) 2024, Rotterdam University of Applied Sciences
 * All rights reserved.
 */

#include <stdint.h>
#include <string.h>
// Driver Header files
#include <NoRTOS.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART2.h>
// Driver configuration
#include "ti_drivers_config.h"

int main(void) {
    Board_init();
    NoRTOS_start();
    // Turn off red LED
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_OFF);
    // Init UART
    UART2_Params uartParams;
    UART2_Params_init(&uartParams);
    uartParams.baudRate = 9600;
    UART2_Handle uart = UART2_open(CONFIG_UART2_0, &uartParams);
    if (uart == NULL) {
        while (1); // UART2_open() failed
    }

    const char echoPrompt[] = "Echoing characters:\r\n";
    size_t bytesWritten;
    UART2_write(uart, echoPrompt, strlen(echoPrompt), &bytesWritten);

    while (1) { // Loop forever echoing
        char input;
        size_t bytesRead;
        UART2_read(uart, &input, 1, &bytesRead);
        UART2_write(uart, &input, 1, &bytesWritten);
        GPIO_toggle(CONFIG_GPIO_LED_0);
    }
}
